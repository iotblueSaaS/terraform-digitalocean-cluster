 variable "pool-name-1" {
     type = string
     description= "pool-name"
 }

 variable "node-count-1" {
     type = string
     description = "node-count"
 }

 variable "label-key" {
   type = string
   description ="label-key"
 }

 variable "label-value" {
   type = string
   description = "label-value"
 }
