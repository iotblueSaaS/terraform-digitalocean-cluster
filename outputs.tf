output "cluster-id" {
    value = digitalocean_kubernetes_cluster.test-terraform.id
    description= "cluster-id"
}

output "node-names" {
    value = digitalocean_kubernetes_node_pool.pool.node.names
    description= "node-names"
}

output "node-id" {
    value = digitalocean_kubernetes_node_pool.pool.node.id
    description= "node-id"
}

output "node-status" {
    value = digitalocean_kubernetes_node_pool.pool.node.status
    description= "node-status"
}